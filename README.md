Forked from https://github.com/petrovich/petrovich-php

All documentation in link above. There is just Composer package.

##### BREAKING CHANGES: 
- Added namespaces (`Petrovich` and `Petrovich\Traits`) 
and renamed classes (from `Trait_Petrovich` to `TraitPetrovich`).
- `petrovich-rules` was cloned to project